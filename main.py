import time
import sys
import RPi.GPIO as GPIO
from parser import ThinkGearParser, TimeSeriesRecorder
from startup import mindwave_startup

# -------------------------------------Configuration-------------------------------------------------------------------#
FREQUENCY = 100
BUZZER_FREQ = 1000
MOTOR_1_SLOW_DUTY = 20
MOTOR_1_FAST_DUTY = 30
MOTOR_2_SLOW_DUTY = 20
MOTOR_2_FAST_DUTY = 30
# -------------------------------------PINS Numbers-------------------------------------------------------------------#
FORWARD_STOP_PIN = 23
BACKWARD_STOP_PIN = 24
MOTOR_1_RIGHT_SPEED = 8
MOTOR_1_LEFT_SPEED = 7
MOTOR_2_RIGHT_SPEED = 27
MOTOR_2_LEFT_SPEED = 22
# Define GPIO to LCD mapping
LCD_D4 = 6
LCD_D5 = 13
LCD_D6 = 19
LCD_D7 = 26
LCD_RS = 20
LCD_E = 21
# Buzzer
BUZZER_PIN = 16
# -------------------------------------initialization-------------------------------------------------------------------#
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
# Set pins Input/Output
GPIO.setup(FORWARD_STOP_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(BACKWARD_STOP_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(MOTOR_1_RIGHT_SPEED, GPIO.OUT)
GPIO.setup(MOTOR_1_LEFT_SPEED, GPIO.OUT)
GPIO.setup(MOTOR_2_RIGHT_SPEED, GPIO.OUT)
GPIO.setup(MOTOR_2_LEFT_SPEED, GPIO.OUT)
GPIO.setup(BUZZER_PIN, GPIO.OUT)
GPIO.setup(LCD_E, GPIO.OUT)  # E
GPIO.setup(LCD_RS, GPIO.OUT) # RS
GPIO.setup(LCD_D4, GPIO.OUT) # DB4
GPIO.setup(LCD_D5, GPIO.OUT) # DB5
GPIO.setup(LCD_D6, GPIO.OUT) # DB6
GPIO.setup(LCD_D7, GPIO.OUT) # DB7
# Set pins as PWM
MOTOR_1_RIGHT_SPEED_PWM = GPIO.PWM(MOTOR_1_RIGHT_SPEED, FREQUENCY)
MOTOR_1_RIGHT_SPEED_PWM.start(0)
MOTOR_1_LEFT_SPEED_PWM = GPIO.PWM(MOTOR_1_LEFT_SPEED, FREQUENCY)
MOTOR_1_LEFT_SPEED_PWM.start(0)
MOTOR_2_RIGHT_SPEED_PWM = GPIO.PWM(MOTOR_2_RIGHT_SPEED, FREQUENCY)
MOTOR_2_RIGHT_SPEED_PWM.start(0)
MOTOR_2_LEFT_SPEED_PWM = GPIO.PWM(MOTOR_2_LEFT_SPEED, FREQUENCY)
MOTOR_2_LEFT_SPEED_PWM.start(0)

BUZZER_PIN_PWM = GPIO.PWM(BUZZER_PIN, BUZZER_FREQ)
BUZZER_PIN_PWM.start(0)
# Define some device constants
LCD_WIDTH = 16  # Maximum characters per line
LCD_CHR = True
LCD_CMD = False

LCD_LINE_1 = 0x80  # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0  # LCD RAM address for the 2nd line

# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005
# -------------------------------------Variables-------------------------------------------------------------------#
current_direction = 'Forward'


# move motors forward
def move_clockwise(motor_1_speed, motor_2_speed):
    MOTOR_1_RIGHT_SPEED_PWM.ChangeDutyCycle(motor_1_speed)
    MOTOR_1_LEFT_SPEED_PWM.ChangeDutyCycle(0)
    MOTOR_2_RIGHT_SPEED_PWM.ChangeDutyCycle(motor_2_speed)
    MOTOR_2_LEFT_SPEED_PWM.ChangeDutyCycle(0)


# move motors backward
def move_counter_clockwise(motor_1_speed, motor_2_speed):
    MOTOR_1_RIGHT_SPEED_PWM.ChangeDutyCycle(0)
    MOTOR_1_LEFT_SPEED_PWM.ChangeDutyCycle(motor_1_speed)
    MOTOR_2_RIGHT_SPEED_PWM.ChangeDutyCycle(0)
    MOTOR_2_LEFT_SPEED_PWM.ChangeDutyCycle(motor_2_speed)


# decide direction and move motors
def move_motors(motor_1_speed, motor_2_speed):
    if current_direction == 'Forward':
        move_clockwise(motor_1_speed, motor_2_speed)
    else:
        move_counter_clockwise(motor_1_speed, motor_2_speed)


# check the attention change and move the motors
def handle_attention_change(attention):
    if attention > 70:
        move_motors(MOTOR_1_FAST_DUTY, MOTOR_2_FAST_DUTY)
        print " , Speed: ", MOTOR_1_FAST_DUTY, " , ", MOTOR_2_FAST_DUTY
    elif attention > 50:
        move_motors(MOTOR_1_SLOW_DUTY, MOTOR_2_SLOW_DUTY)
        print " , Speed: ", MOTOR_1_SLOW_DUTY, " , ", MOTOR_2_SLOW_DUTY
    else:
        move_motors(0, 0)
        print " , Speed: 0 , 0"


# check the stop pins
def check_stop_pins():
    global current_direction
    if not GPIO.input(FORWARD_STOP_PIN):
        current_direction = 'Backward'
    elif not GPIO.input(BACKWARD_STOP_PIN):
        current_direction = 'Forward'
    print " , Direction: ", current_direction,


# connect to bluetooth device
def connect():
    extra_args = [
        dict(
            name='measure',
            type=str,
            nargs='?',
            const="attention",
            default="attention",
            help="""Measure you want feedback on. Either "meditation" or "attention\""""
        )
    ]
    mindwave_socket, args = mindwave_startup(
        description="Simple Neuro feedback console application",
        extra_args=extra_args
    )

    # validate measure argument
    if args.measure not in ["attention", "meditation"]:
        print "Unknown measure %s" % repr(args.measure)
        sys.exit(-1)

    # create recorder, parser instances
    recorder_instance = TimeSeriesRecorder()
    parser_instance = ThinkGearParser(recorders=[recorder_instance])

    return mindwave_socket, args.measure, parser_instance, recorder_instance


# read new value
def read(mindwave_socket, measure, parser_instance, recorder_instance):
    data = mindwave_socket.recv(20000)
    parser_instance.feed(data)
    if measure == 'attention':
        if len(recorder_instance.attention) > 0:
            return recorder_instance.attention[-1]
    if measure == 'meditation':
        if len(recorder_instance.meditation) > 0:
            return recorder_instance.meditation[-1]
    return 0


def lcd_init():
    # Initialise display
    lcd_byte(0x33, LCD_CMD)  # 110011 Initialise
    lcd_byte(0x32, LCD_CMD)  # 110010 Initialise
    lcd_byte(0x06, LCD_CMD)  # 000110 Cursor move direction
    lcd_byte(0x0C, LCD_CMD)  # 001100 Display On,Cursor Off, Blink Off
    lcd_byte(0x28, LCD_CMD)  # 101000 Data length, number of lines, font size
    lcd_byte(0x01, LCD_CMD)  # 000001 Clear display
    time.sleep(E_DELAY)


def lcd_byte(bits, mode):
    # Send byte to data pins
    # bits = data
    # mode = True  for character
    #        False for command

    GPIO.output(LCD_RS, mode)  # RS

    # High bits
    GPIO.output(LCD_D4, False)
    GPIO.output(LCD_D5, False)
    GPIO.output(LCD_D6, False)
    GPIO.output(LCD_D7, False)
    if bits & 0x10 == 0x10:
        GPIO.output(LCD_D4, True)
    if bits & 0x20 == 0x20:
        GPIO.output(LCD_D5, True)
    if bits & 0x40 == 0x40:
        GPIO.output(LCD_D6, True)
    if bits & 0x80 == 0x80:
        GPIO.output(LCD_D7, True)

    # Toggle 'Enable' pin
    lcd_toggle_enable()

    # Low bits
    GPIO.output(LCD_D4, False)
    GPIO.output(LCD_D5, False)
    GPIO.output(LCD_D6, False)
    GPIO.output(LCD_D7, False)
    if bits & 0x01 == 0x01:
        GPIO.output(LCD_D4, True)
    if bits & 0x02 == 0x02:
        GPIO.output(LCD_D5, True)
    if bits & 0x04 == 0x04:
        GPIO.output(LCD_D6, True)
    if bits & 0x08 == 0x08:
        GPIO.output(LCD_D7, True)

    # Toggle 'Enable' pin
    lcd_toggle_enable()


def lcd_toggle_enable():
    # Toggle enable
    time.sleep(E_DELAY)
    GPIO.output(LCD_E, True)
    time.sleep(E_PULSE)
    GPIO.output(LCD_E, False)
    time.sleep(E_DELAY)


def lcd_string(message, line):
    # Send string to display

    message = message.ljust(LCD_WIDTH, " ")

    lcd_byte(line, LCD_CMD)

    for i in range(LCD_WIDTH):
        lcd_byte(ord(message[i]), LCD_CHR)


if __name__ == '__main__':
    # connect to device
    socket, measure_name, parser, recorder = connect()
    lcd_init()
    while 1:
        time.sleep(0.25)
        # read new value
        value = read(socket, measure_name, parser, recorder)
        print "Value: ", value,
        if value > 0:
            # check stop pins
            check_stop_pins()
            # move the motors depending on attention value
            handle_attention_change(value)
            BUZZER_PIN_PWM.ChangeDutyCycle(0)
        else:
            BUZZER_PIN_PWM.ChangeDutyCycle(50)
            print ""
        lcd.clear()
        if value > 50:
            lcd_string("High attention")
        else:
            lcd_string("Low attention")
